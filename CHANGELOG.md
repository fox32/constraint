## 0.2.0

* Ignore private members

Note: private members cannot be accessed (when in different libraries).
This release is only backwards incompatible if you put constraints on private
members

## 0.1.4+3

* Fixed bug when running dart2js. Thanks to Robert Akerblom-Andersson for the contribution


## 0.1.4+2

* Fixed bug in core constraints that stopped them validating without a group. Thanks to Robert Akerblom-Andersson for the contribution

## 0.1.4+1

* Exported Pattern from constrain.dart

## 0.1.4

* Added Min, Max and Pattern as core constraints

## 0.1.3

* Added ConstrainedFunctionProxy as a reflective wrapper for validating functions

## 0.1.2

* Added support for constraining functions and methods
    * both parameters and return values can be constrained and validated
    * methods inherit constraints from super classes, interfaces and mixins
    * parameters and return values will be deeply validated, including all 
        constraints on type
    * for example

```
class Foo {
  String bar(@NotNull() int blah, String foo) => '$blah';
}

class Blah extends Object with Foo {
  @NotNull() String bar(@Ensure(isBetween10and90) int blah,
                          @NotNull() String foo) => '$blah';
}
```

## 0.1.1+1

* Bug Fix. Ignore static fields and methods

## 0.1.1

* JSON support. Constraint violations can be converted to JSON, 
for example to send to the client

## 0.1.0+1

* Fixed bugs with mirrors

## 0.1.0
Implemented most features. Should be highly useable now. Added

* Class based constraints
* handling of collections
* validator functions
* groups
* much more

## 0.0.1

* Basic strawman
