// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.core_constraints;


export 'src/core_constraints/min_max.dart';
export 'src/core_constraints/pattern.dart';

// TODO: add all the constraints that are likely to be needed when integrating
// with other formats (e.g. Json Schema) and frameworks

/*
1. Null ??
1. Min({bool inclusive: true}) => done
1. Max({bool inclusive: true}) => done
1. Digits ????
1. Size
1. Pattern => done
1. uniqueItems
1. fractionDigits ???
1. totalDigits ???
1. AssertFalse  ???
1. AssertTrue ???
1. Past ???
1. Future ???

class Range extends Constraint {
  num lowerBound;
  num upperBound;

  @ override
  String get descriptionTemplate => '$propertyName must be between $lowerBound and $upperBound';
}
 *
javax.validation.constraints.AssertFalse.message = must be false
javax.validation.constraints.AssertTrue.message  = must be true
javax.validation.constraints.DecimalMax.message  = must be less than ${inclusive == true ? 'or equal to ' : ''}{value}
javax.validation.constraints.DecimalMin.message  = must be greater than ${inclusive == true ? 'or equal to ' : ''}{value}
javax.validation.constraints.Digits.message      = numeric value out of bounds (<{integer} digits>.<{fraction} digits> expected)
javax.validation.constraints.Future.message      = must be in the future
javax.validation.constraints.Max.message         = must be less than or equal to {value}
javax.validation.constraints.Min.message         = must be greater than or equal to {value}
javax.validation.constraints.NotNull.message     = may not be null
javax.validation.constraints.Null.message        = must be null
javax.validation.constraints.Past.message        = must be in the past
javax.validation.constraints.Pattern.message     = must match "{regexp}"
javax.validation.constraints.Size.message        = size must be between {min} and {max}
*/


