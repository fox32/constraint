// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constrain.validator.function.proxy;

import 'dart:mirrors';
import 'package:constrain/constraint.dart';
import 'package:constrain/validator.dart';
import 'dart:async';


final TypeMirror _futureType = reflectType(Future);

class ConstrainedFunctionProxy {
  final Validator validator;

  ConstrainedFunctionProxy(this.validator);

  invoke(ClosureMirror cm,
      List positionalParameters,
      { Map<Symbol, dynamic> namedArguments: const {},
        Iterable<ConstraintGroup> groups,
        bool validateParameters: true, bool validateReturn: false }) {


    if (validateParameters) {
      final violations = validator.validateFunctionParameters(cm.reflectee,
          positionalParameters, namedParameters: namedArguments, groups: groups);

      if (violations.isNotEmpty) {
        if (cm.function.returnType.isSubtypeOf(_futureType)) {
          return new Future.error(new ParameterConstraintViolationException(violations));
        }
        else {
          throw new ParameterConstraintViolationException(violations);
        }
      }
    }

    final result = cm.apply(positionalParameters, namedArguments).reflectee;

    // don't want to turn a non future into a future
    return result is Future ? result.then((r) =>
        _handleReturn(cm, validateReturn, r, groups))
        : _handleReturn(cm, validateReturn, result, groups);

  }

  _handleReturn(ClosureMirror cm, bool validateReturn, result, groups) {
    if (validateReturn) {
      final Set<ConstraintViolation> returnViolations =
          validator.validateFunctionReturn(cm.reflectee, result, groups: groups);

      if (returnViolations.isNotEmpty) {
        throw new ReturnConstraintViolationException(returnViolations);
      }
    }
    return result;

  }
}

class ParameterConstraintViolationException extends ConstraintViolationException {
  ParameterConstraintViolationException(Set<ConstraintViolation> violations)
      : super(violations);
}

class ReturnConstraintViolationException extends ConstraintViolationException {
  ReturnConstraintViolationException(Set<ConstraintViolation> violations)
      : super(violations);
}

// TODO: remove
class Foo {
  String blah(int bar) => "";
}

@proxy class FooProxy extends MethodValidatingProxy implements Foo {
  FooProxy(ConstrainedFunctionProxy functionProxy, Foo delegate)
      : super(functionProxy, delegate);

  noSuchMethod(i) => super.noSuchMethod(i);
}

class MethodValidatingProxy  {
  final ConstrainedFunctionProxy _functionProxy;
  final delegate;
  final InstanceMirror _delegateMirror;

  MethodValidatingProxy(this._functionProxy, delegate)
      : this.delegate = delegate,
        _delegateMirror = reflect(delegate);

  noSuchMethod(Invocation invocation) {
//    final method = _delegateMirror.type.declarations[invocation.memberName];
    final method = _delegateMirror.getField(invocation.memberName);

    if (method == null) {
      throw new NoSuchMethodError(delegate, invocation.memberName,
          invocation.positionalArguments, invocation.namedArguments);
    }

    return _functionProxy.invoke(method, invocation.positionalArguments,
        namedArguments: invocation.namedArguments);
  }
}



