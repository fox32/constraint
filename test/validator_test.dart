// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.validator.test;

import 'package:constrain/validator.dart';
import 'package:unittest/unittest.dart';
import 'package:constrain/constraint.dart';
import 'domain_model.dart';

main() {
  group('validate', () {
    Validator v = new Validator();

    group('should return empty set', () {
      test('when validated object is null', () {
        expect(v.validate(null), isEmpty);
      });

      test('when validated object has no constraints', () {
        expect(v.validate(new MrNoConstraints()..name="fred"), isEmpty);
      });

      test('when field is valid according to constraint', () {
        expect(v.validate(new TestModel1("fred")), isEmpty);
      });

      test('when non mandatory field is null', () {
        expect(v.validate(new TestModel2()), isEmpty);
      });

      test('when non mandatory field is valid according to constrain', () {
        expect(v.validate(new TestModel2("Mr.")), isEmpty);
      });
    });

    group('should return non empty set', () {
      final person = new TestModel1(null);

      Set<ConstraintViolation> validate() => v.validate(person);

      test('when non mandatory field is invalid according to constrain', () {
        expect(v.validate(new TestModel2("Mr")), hasLength(1));
      });

      test('when field is invalid according to constraint', () {
        expect(validate(), hasLength(1));
      });

      test('with a violation containing the correct constraint', () {
        expect(validate().first.constraintDescriptor.constraint,
            new isInstanceOf<Ensure>());
      });

      test('with a violation containing the correct root', () {
        expect(validate().first.rootObject, equals(person));
      });

      test('with a violation containing the correct leaf', () {
        expect(validate().first.leafObject, equals(person));
      });

      test('with a violation containing the correct path', () {
        expect(validate().first.propertyPath, hasLength(1));
        expect(validate().first.propertyPath, contains(#name));
      });

      test('with a message', () {
        expect(validate().first.message, isNotNull);
      });
    });

    group('should validate child model', () {
      TestModel1 test1(String name) => new TestModel1(name);
      TestModel3 test3(String name, String title) =>
          new TestModel3(title, test1(name));

      Set<ConstraintViolation> validate(String name, String title) =>
          v.validate(test3(name, title));

      test('and return an empty set if valid', () {
        expect(validate("fred", "Mr."), isEmpty);
      });

      test('and return a non empty set if invalid', () {
        expect(validate(null, "Mr."), hasLength(1));
      });

      test('with correct path', () {
        expect(validate(null, "Mr.").first.propertyPath, hasLength(2));
        expect(validate(null, "Mr.").first.propertyPath.first, equals(#testModel1));
        expect(validate(null, "Mr.").first.propertyPath.last, equals(#name));
      });

      test('and return a set with two elements if invalid both invalid', () {
        expect(validate(null, "Mr"), hasLength(2));
      });
    });

    group('should validate class level constraints', () {
      runTest(Set<ConstraintViolation> validate(int blah)) {
        test('and return empty set is valid', () {
          expect(validate(null), isEmpty);
          expect(validate(10), isEmpty);
        });

        test('and return non empty set is invalid', () {
          expect(validate(-10), isNot(isEmpty));
        });
      }

      group('on directly validated class', () {
        Set<ConstraintViolation> validate(int blah) =>
            v.validate(new AModelWithClassLevelConstraint(blah));

        runTest(validate);
      });

      group('on child class', () {
        Set<ConstraintViolation> validate(int blah) =>
            v.validate(new Type3(new AModelWithClassLevelConstraint(blah)));

        runTest(validate);
      });
    });

    group('should cope with loops', () {
      group('if they are just type loops', () {
        test('and return empty on valid', () {
          final type2a = new Type2();
          final type2b = new Type2();
          final type1 = new Type1();
          type2a.type1 = type1;
          type1.type2 = type2b;

          expect(v.validate(type2a), isEmpty);
        });

        test('and return non empty on invalid', () {
          final type1a = new Type1();
          final type1b = new Type1();
          final type2 = new Type2();
          type1a.type2 = type2;
          type2.type1 = type1b;

          expect(v.validate(type1a), isNot(isEmpty));
        });
      });

      test('if they are actual loops in validated object graph', () {
        final type2a = new Type2();
        final type1 = new Type1();
        type2a.type1 = type1;
        type1.type2 = type2a;

        expect(v.validate(type2a), isEmpty);
      });
    });

    group('should detect collections', () {
      group('and return non empty on invalid', () {
        final testModel4 = new TestModel4();
        final testModel1a = new TestModel1('fred');
        final testModel1b = new TestModel1(null);
        testModel4.models = [testModel1a, testModel1b];
        ConstraintViolation violation() => v.validate(testModel4).first;

        test('', () {
          expect(v.validate(testModel4), isNot(isEmpty));
          expect(v.validate(testModel4), hasLength(1));
        });

        test('with correct constraint descriptor', () {
          expect(violation().constraintDescriptor.constraint.validator,
              equals(isNotNull));
        });

        test('with correct invalid value', () {
          expect(violation().invalidValue, isNull);
        });

        test('with correct leaf object', () {
          expect(violation().leafObject, equals(testModel1b));
        });

        test('with correct root object', () {
          expect(violation().rootObject, equals(testModel4));
        });
      });

      test('and cope with null collections', () {
        expect(v.validate(new TestModel4()), isEmpty);
      });
    });

    group('should support ConstraintValidators', () {
      final Person person = new Person()
        ..age = 22
        ..addresses = [new Address()..street = "15 foo bar st"];

      person.parents = new Set()..add(person);

      validate() => v.validate(person);

      test('and return non empty when invalid', () {
        expect(validate(), isNot(isEmpty));
        expect(validate(), hasLength(1));
      });

      test('and include description in violation', () {
        expect(validate().first.constraintDescriptor.constraint.description,
            equals('A person cannot be their own parent'));
      });

    });

    group('should support SimpleConstraintValidators', () {

      test('and return empty when valid', () {
        final model = new TestModel5()..name = 'foXo';

        Set<ConstraintViolation> validate() => v.validate(model);

        expect(validate(), isEmpty);
      });

      test('and return non empty when invalid', () {
        final model = new TestModel5()..name = 'foo';

        Set<ConstraintViolation> validate() => v.validate(model);

        expect(validate(), isNot(isEmpty));
        expect(validate(), hasLength(1));
      });

    });

    group('should only validate constraints implied by group', () {
      final model = new TestModel6();

      Set<ConstraintViolation> validate([List<ConstraintGroup> groups]) =>
          v.validate(model, groups: groups);

      Set<ConstraintViolation> validateAndFilter(
          ConstraintGroup validateGroup, ConstraintGroup filterGroup) {
              return validate([validateGroup]).where((cv) =>
              cv.constraintDescriptor.constraint.group == filterGroup).toSet();
      }

      Set<ConstraintViolation> validateOne() => validate([const GroupOne()]);

      group('which when not provided defaults to DefaultGroup', () {

        test('and returns a non empty result when invalid', () {
          expect(validate(), isNot(isEmpty));
          expect(validate(), hasLength(1));
        });

        test('and returns a violation on that group', () {
          expect(validate().first.constraintDescriptor.constraint.group,
              equals(const DefaultGroup()));
        });
      });

      group('which when provided includes that group and DefaultGroup', () {
        test('and returns a non empty result when invalid', () {
          expect(validateOne(), isNot(isEmpty));
          expect(validateOne(), hasLength(2));
        });

        test('and returns a violation on default group', () {
          expect(validateAndFilter(const GroupOne(), const DefaultGroup()),
              hasLength(1));
        });

        test('and returns a violation on that group', () {
          expect(validateAndFilter(const GroupOne(), const GroupOne()),
              hasLength(1));
        });
      });

      group('which when a super provided includes all groups implied by that group and DefaultGroup', () {
        final groups = [const GroupOne(), const GroupTwo()];

        test('and returns a non empty result when invalid', () {
          expect(validate(groups), isNot(isEmpty));
          expect(validate(groups), hasLength(3));
        });

        test('and returns a violation on default group', () {
          expect(validateAndFilter(const GroupOneAndTwo(), const DefaultGroup()),
              hasLength(1));
        });

        test('and returns a violation on first group', () {
          expect(validateAndFilter(const GroupOneAndTwo(), const GroupOne()),
              hasLength(1));
        });

        test('and returns a violation on second group', () {
          expect(validateAndFilter(const GroupOneAndTwo(), const GroupTwo()),
              hasLength(1));
        });
      });
    });

  });


}
