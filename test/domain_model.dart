// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constrain.test.domain;

import 'package:constrain/core_constraints.dart';
import 'package:constrain/constraint.dart';
import 'package:matcher/matcher.dart';
import 'package:constrain/matchers.dart';
import 'dart:core' hide Pattern;
//import 'dart:core' as core show Pattern;

class MrNoConstraints {
  String name;
}

class MrOnlyStaticConstraints {
  String name;

  @NotNull()
  static String shouldBeIgnored;

  @NotNull()
  static String get shouldBeIgnored2 => '';

}


class TestModel1 {
  @Ensure(isNotNull)
  final String name;

  TestModel1(this.name);

  String toString() => '$runtimeType[name: $name]';
}

class TestModel2 {
  @Ensure(endsWithDot)
  final String title;

  TestModel2([this.title]);

  String toString() => '$runtimeType[name: $title]';
}

Matcher endsWithDot() => endsWith('.');

class TestModel3 {
  @Ensure(endsWithDot)
  final String title;

  final TestModel1 testModel1;

  TestModel3([this.title, this.testModel1]);

  String toString() => '$runtimeType[name: $title, test1: $testModel1]';
}

class ChildModel extends TestModel1 {
  ChildModel(String name) : super(name);
}

class ChildModel2 extends TestModel1 {
  @Ensure(endsWithDot)
  String get name => super.name;

  ChildModel2(String name) : super(name);

}

class MyConstraint extends Constraint<String> {
  const MyConstraint();
  @override
  void validate(String value, ConstraintValidationContext context) {
  }
}

class MyTestModel {
  @MyConstraint()
  String name;
}

class Type1 {
  @Ensure(isNotNull)
  Type2 type2;
}

class Type2 {
  Type1 type1;
}

@Ensure(blahIsNullOrPositive)
class AModelWithClassLevelConstraint {
  int blah;

  AModelWithClassLevelConstraint([this.blah]);
}

class Type3 {
  AModelWithClassLevelConstraint model;

  Type3([this.model]);
}

class ChildModel3 extends AModelWithClassLevelConstraint {}

@Ensure(blahInRange)
class ChildModel4 extends AModelWithClassLevelConstraint {}

class ChildModel5 implements TestModel1 {
  @Ensure(endsWithDot)
  @override
  String name;
}

@Ensure(blahInRange)
class ChildModel6 implements AModelWithClassLevelConstraint {
  int blah;
}

@Ensure(blahIsNullOrPositive)
class AModelWithClassLevelConstraint2 {
  @Ensure(isNotNull)
  int blah;
}


class ChildModel7 extends Object
    with AModelWithClassLevelConstraint2 {
}
//class ChildModel7 extends Object
//    with AModelWithClassLevelConstraint2, TestModel1b {
//}

class TestModel4 {
  List<TestModel1> models;
}

class TestModel5 {
  @Ensure(containsAnX)
  String name;

  static bool containsAnX(String name, TestModel5 owner) =>
      name.contains('X');
}

class GroupOne extends SimpleConstraintGroup {
  const GroupOne();
}

class GroupTwo extends SimpleConstraintGroup {
  const GroupTwo();
}

class GroupOneAndTwo extends CompositeGroup {
  const GroupOneAndTwo() : super(const [const GroupOne(), const GroupTwo()]);
}

class TestModel6 {
  @NotNull(group: const GroupOne())
  String one;

  @NotNull(group: const GroupTwo())
  String two;

  @NotNull()
  String three;
}

Matcher blahIsNullOrPositive() =>
    fieldMatcher("AModelWithClassLevelConstraint", "blah",
          anyOf(isNull, isPositive),
        (AModelWithClassLevelConstraint c) => c.blah);

Matcher blahInRange() =>
    fieldMatcher("AModelWithClassLevelConstraint", "blah",
          anyOf(isNull, inInclusiveRange(12, 56)),
        (AModelWithClassLevelConstraint c) => c.blah);



class Primate {
  @Ensure(isPositive)
  int age;
}

@Ensure(eitherOlderThan20OrHasAtLeastTwoAddresses,
    description: 'Must be either older than twenty or have at least two adresses')
class Person extends Primate {
  @Ensure(isBetween10and90)
  int get age => super.age;

  @NotNull()
  @Ensure(isNotEmpty)
  @Ensure(allStreetsStartWith15, group: const Detailed())
  List<Address> addresses;

  @Ensure(cantBeYourOwnParent,
      description: "A person cannot be their own parent")
  Set<Person> parents;


  String toString() => 'Person[age: $age, addressses: $addresses]';

  Map toJson() => {
    'age': age,
    'parents': _iterableToJson(parents),
    'addresses': _iterableToJson(addresses)
  };
}

_iterableToJson(Iterable i) =>
    i == null ? null : i.map((p) => p.toJson()).toList();

class Address {
  @Ensure(streetIsAtLeast10Characters)
  String street;

  String toString() => 'Address[street: $street]';

  Map toJson() => {
    'street': street
  };
}

class Detailed extends SimpleConstraintGroup {
  const Detailed();
}



/*
 * constraints
 */


// Person constraints

bool cantBeYourOwnParent(Set<Person> parents, Person person) =>
    !parents.contains(person);

Matcher isBetween10and90() =>
    allOf(greaterThanOrEqualTo(10), lessThanOrEqualTo(90));

bool eitherOlderThan20OrHasAtLeastTwoAddresses(Person person) =>
    person.age == null || person.addresses == null ||
    person.age > 20 || person.addresses.length >= 2;

bool allStreetsStartWith15New(List<Address> addresses) =>
  addresses.every((a) => a.street == null || a.street.startsWith("15"));

Matcher allStreetsStartWith15() => everyElement(_hasStreet(startsWith("15")));

Matcher hasSelf(matcher) => _personMatcher("this", matcher,
    (Person p) => p.parents);

Matcher hasParents(matcher) => _personMatcher("parents", matcher,
    (Person p) => p.parents);

Matcher hasAge(matcher) => _personMatcher("age", matcher, (Person p) => p.age);

Matcher hasAddresses(matcher) => _personMatcher("addresses", matcher,
    (Person p) => p.addresses);

Matcher _personMatcher(String fieldName, matcher, Getter getter)
    => fieldMatcher("Person", fieldName, matcher, getter);


// Address constraints

Matcher streetIsAtLeast10Characters() => hasLength(greaterThanOrEqualTo(10));

Matcher _hasStreet(matcher) => _addressMatcher("street", matcher,
    (Address a) => a.street);

Matcher _addressMatcher(String fieldName, matcher, Getter getter)
    => fieldMatcher("Address", fieldName, matcher, getter);


//DateTime DAWN_OF_TIME() => new DateTime(1900);

class FooMan {
  @Min(10)
  @Max(90)
  int a;

  @Min(10.0)
  @Max(90.0, isInclusive: false)
  double b;

//  @Min(const DateTime(1900))
//  @Max(90, isInclusive: false)
  DateTime d;

  @Pattern(r'd')
  String s;

}

class PrivateField {
  @NotNull()
  String _name;

  @NotNull()
  int age;
}

class PrivateAlso {
  @NotNull()
  PrivateField priv;
}