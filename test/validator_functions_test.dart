// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.validator.function.test;

import 'package:constrain/validator.dart';
import 'package:unittest/unittest.dart';
import 'package:constrain/constraint.dart';
import 'domain_model.dart';

main() {
  group('validateFunctionParameters', () {
    Validator v = new Validator();

    group('should return empty set', () {
      test('when validated object has no constraints', () {
        expect(v.validateFunctionParameters(mrNoConstraints, [1]), isEmpty);
      });

      test('when parameter is valid according to constraint', () {
        expect(v.validateFunctionParameters(singleConstraints, [11, "d"]), isEmpty);
      });

      test('when non mandatory parameter is null', () {
        expect(v.validateFunctionParameters(singleConstraints, [null, ""]), isEmpty);
      });
    });

    group('should return non empty set', () {
      Set<ConstraintViolation> validate(int blah, String foo) =>
          v.validateFunctionParameters(singleConstraints, [blah, foo]);

      Set<ConstraintViolation> validateFail() => validate(9, "g");

      test('when parameter is invalid according to constraint', () {
        expect(validate(11, null), hasLength(1));
      });

      test('when non mandatory parameter is invalid according to constraint', () {
        expect(validateFail(), hasLength(1));
      });

      test('with a violation containing the correct constraint', () {
        expect(validateFail().first.constraintDescriptor.constraint,
            new isInstanceOf<Ensure>());
      });

      test('with a violation containing the correct root', () {
        expect(validateFail().first.rootObject, equals(singleConstraints));
      });

      test('with a violation containing the correct leaf', () {
        expect(validateFail().first.leafObject, equals(singleConstraints));
      });

      test('with a violation containing the correct path', () {
        expect(validateFail().first.propertyPath, hasLength(1));
        expect(validateFail().first.propertyPath, contains(#blah));
      });

      test('with a message', () {
        expect(validateFail().first.message, isNotNull);
      });
    });

    group('should return non empty set', () {
      Set<ConstraintViolation> validate(int blah, String foo) =>
          v.validateFunctionParameters(singleNamedConstraint, [foo],
              namedParameters: { #blah: blah });

      Set<ConstraintViolation> validateFail() => validate(null, "g");

      test('when named parameter is invalid according to constraint', () {
        expect(validate(null, "ff"), hasLength(1));
      });

      test('when non mandatory parameter is invalid according to constraint', () {
        expect(validateFail(), hasLength(1));
      });

      test('with a violation containing the correct constraint', () {
        expect(validateFail().first.constraintDescriptor.constraint,
            new isInstanceOf<NotNull>());
      });

      test('with a violation containing the correct root', () {
        expect(validateFail().first.rootObject, equals(singleNamedConstraint));
      });

      test('with a violation containing the correct leaf', () {
        expect(validateFail().first.leafObject, equals(singleNamedConstraint));
      });

      test('with a violation containing the correct path', () {
        expect(validateFail().first.propertyPath, hasLength(1));
        expect(validateFail().first.propertyPath, contains(#blah));
      });

      test('with a message', () {
        expect(validateFail().first.message, isNotNull);
      });
    });

    // TODO: named parameters

  });

  group('validateFunctionReturn', () {
    Validator v = new Validator();

    group('should return empty set', () {
      test('when validated object has no constraints', () {
        expect(v.validateFunctionReturn(mrNoConstraints, "foo"), isEmpty);
      });

      test('when parameter is valid according to constraint', () {
        expect(v.validateFunctionReturn(constraintOnReturn, "foo"), isEmpty);
      });

      test('when non mandatory parameter is null', () {
        expect(v.validateFunctionReturn(mrNoConstraints, null), isEmpty);
      });
    });

    group('should return non empty set', () {
      Set<ConstraintViolation> validate(String returnValue) =>
          v.validateFunctionReturn(constraintOnReturn, returnValue);

      Set<ConstraintViolation> validateFail() => validate(null);

      test('when return is invalid according to constraint', () {
        expect(validateFail(), hasLength(1));
      });

      test('with a violation containing the correct constraint', () {
        expect(validateFail().first.constraintDescriptor.constraint,
            new isInstanceOf<NotNull>());
      });

      test('with a violation containing the correct root', () {
        expect(validateFail().first.rootObject, equals(constraintOnReturn));
      });

      test('with a violation containing the correct leaf', () {
        expect(validateFail().first.leafObject, equals(constraintOnReturn));
      });

      test('with a violation containing the correct path', () {
        expect(validateFail().first.propertyPath, hasLength(1));

        // TODO: what should we do about path on a return???
//        expect(validateFail().first.propertyPath, contains(#___return));
      });

      test('with a message', () {
        expect(validateFail().first.message, isNotNull);
      });
    });

    group('should return non empty set', () {
      final testModel = new TestModel3("Dr");
      Set<ConstraintViolation> validate() =>
          v.validateFunctionReturn(constraintOnReturnType, testModel);

      Set<ConstraintViolation> validateFail() => validate();

      test('when return is invalid according to constraint on return type', () {
        expect(validateFail(), hasLength(1));
      });

      test('with a violation containing the correct constraint', () {
        expect(validateFail().first.constraintDescriptor.constraint,
            new isInstanceOf<Ensure>());
      });

      test('with a violation containing the correct root', () {
        expect(validateFail().first.rootObject, equals(constraintOnReturnType));
      });

      test('with a violation containing the correct leaf', () {
        expect(validateFail().first.leafObject, equals(testModel));
      });

      test('with a violation containing the correct path', () {
        expect(validateFail().first.propertyPath, hasLength(2));

        // TODO: what should we do about path on a return???
//        expect(validateFail().first.propertyPath, contains(#___return));
      });

      test('with a message', () {
        expect(validateFail().first.message, isNotNull);
      });
    });


    group('should return non empty set', () {
      final testModel = new TestModel3("Dr");
      Set<ConstraintViolation> validate() =>
          v.validateFunctionReturn(dynamicReturn, testModel);

      Set<ConstraintViolation> validateFail() => validate();

      test('when return is invalid even when return type dynamic', () {
        expect(validateFail(), hasLength(1));
      });

      test('with a violation containing the correct constraint', () {
        expect(validateFail().first.constraintDescriptor.constraint,
            new isInstanceOf<Ensure>());
      });

      test('with a violation containing the correct root', () {
        expect(validateFail().first.rootObject, equals(dynamicReturn));
      });

      test('with a violation containing the correct leaf', () {
        expect(validateFail().first.leafObject, equals(testModel));
      });

      test('with a violation containing the correct path', () {
        expect(validateFail().first.propertyPath, hasLength(1));

        // TODO: what should we do about path on a return???
//        expect(validateFail().first.propertyPath, contains(#___return));
      });

      test('with a message', () {
        expect(validateFail().first.message, isNotNull);
      });
    });

  });
}


String mrNoConstraints(int blah) => '$blah';

String singleConstraints(@Ensure(isBetween10and90) int blah,
                        @NotNull() String foo) => '$blah';

String constraintOnParamType(TestModel3 model) => '';

String singleNamedConstraint(String foo, { @NotNull() int blah } ) => '$blah';

@NotNull() String constraintOnReturn() => '';

TestModel3 constraintOnReturnType() => null;

dynamicReturn() => null;

