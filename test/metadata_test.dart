// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.metadata.test;

import 'package:constrain/metadata.dart';
import 'package:unittest/unittest.dart';
import 'package:option/option.dart';
import 'domain_model.dart';

main() {
    TypeDescriptorResolver t = new TypeDescriptorResolver();

    group('resolveFor', () {
      test('when class has no constraints should return None', () {
        expect(t.resolveFor(MrNoConstraints), equals(const None()));
      });

      test('when class has only constraints on static should return None', () {
        expect(t.resolveFor(MrOnlyStaticConstraints), equals(const None()));
      });


      group('when class has a member constraint should return a TypeDescriptor', () {
        desc() => t.resolveFor(MyTestModel).get();

        test('that is not null', () {
          expect(desc(), isNotNull);
        });

        test('with correct type', () {
          expect(desc().type, equals(MyTestModel));
        });

        test('with empty constraint descriptors', () {
          expect(desc().constraintDescriptors, isEmpty);
        });

        test('with non empty member descriptors', () {
          expect(desc().memberDescriptors, isNot(isEmpty));
        });

        test('with non empty member descriptors map', () {
          expect(desc().memberDescriptorsMap, isNot(isEmpty));
        });

        test('with a descriptor on the member', () {
          expect(desc().memberDescriptorsMap[#name], isNotNull);
        });

        test('with one constraint on member', () {
          expect(desc().memberDescriptorsMap[#name].constraintDescriptors,
              hasLength(1));
        });

        test('with expected constraint on member', () {
          expect(desc().memberDescriptorsMap[#name].constraintDescriptors.first
                .constraint,
              new isInstanceOf<MyConstraint>());
        });
      });

      group('when child class has a member constraint should return a TypeDescriptor', () {
        desc() => t.resolveFor(TestModel3).get();

        test('that is not null', () {
          expect(desc(), isNotNull);
        });

        test('with correct type', () {
          expect(desc().type, equals(TestModel3));
        });

        test('with empty constraint descriptors', () {
          expect(desc().constraintDescriptors, isEmpty);
        });

        test('with two member descriptors', () {
          expect(desc().memberDescriptors, hasLength(2));
        });

        test('with a descriptor on the member', () {
          expect(desc().memberDescriptorsMap[#testModel1], isNotNull);
        });

        test('with no constraint on member', () {
          expect(desc().memberDescriptorsMap[#testModel1].constraintDescriptors,
              isEmpty);
        });

        test('with a type descriptor on member', () {
          expect(desc().memberDescriptorsMap[#testModel1].typeDescriptor,
              new isInstanceOf<Some>());
        });

        group('with a member TypeDescriptor', () {
          childDesc() =>
              desc().memberDescriptorsMap[#testModel1].typeDescriptor.get();

          test('with no constraint on member type', () {
            expect(childDesc().constraintDescriptors, isEmpty);
          });

          test('with one member descriptors', () {
            expect(childDesc().memberDescriptors, hasLength(1));
          });

          test('with a descriptor on the member', () {
            expect(childDesc().memberDescriptorsMap[#name], isNotNull);
          });

          test('with one constraint on member', () {
            expect(childDesc().memberDescriptorsMap[#name].constraintDescriptors,
                hasLength(1));
          });

          test('with expected constraint on member', () {
            expect(childDesc().memberDescriptorsMap[#name]
                  .constraintDescriptors.first.constraint,
                equals(isNotNull));
          });
        });
      });
      group('should cache type descriptors', () {

        test('and return cached instance on second request', () {
          desc() => t.resolveFor(MyTestModel).get();
          expect(desc(), same(desc()));
        });

        test('and cope with type loops', () {
          type2Desc() => t.resolveFor(Type2).get();
          type1Desc() => type2Desc().memberDescriptorsMap[#type1]
            .typeDescriptor.get();
          type2ViaType1Desc() => type1Desc().memberDescriptorsMap[#type2]
            .typeDescriptor.get();

          expect(type2Desc().type, same(type2ViaType1Desc().type));
        });
      });

      group('should include class level constraints', () {

        group('on looked up class', () {
          descOpt() => t.resolveFor(AModelWithClassLevelConstraint);
          desc() => descOpt().get();

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
            expect(desc().constraintDescriptors, hasLength(1));
          });

          test('with right type', () {
            expect(desc().constraintDescriptors.first.constraint.validator(),
                new isInstanceOf<Matcher>());
          });
        });

        group('on child class', () {
          descOpt() => t.resolveFor(Type3).get()
              .memberDescriptorsMap[#model].typeDescriptor;
          desc() => descOpt().get();

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
            expect(desc().constraintDescriptors, hasLength(1));
          });

          test('with right type', () {
            expect(desc().constraintDescriptors.first.constraint.validator(),
                new isInstanceOf<Matcher>());
          });
        });
      });

      group('should include inherited member constraints', () {
        group('when no constraints on current class', () {
          descOpt() => t.resolveFor(ChildModel);
          desc() => descOpt().get();
          mm() => desc().memberDescriptorsMap;
          nameConstraints() => mm()[#name].constraintDescriptors;

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
          });

          test('and provide access in members map', () {
            expect(mm(), hasLength(1));
            expect(mm()[#name], isNotNull);
            expect(nameConstraints(), hasLength(1));
          });

        });

        group('when subclass has additional constraints on property', () {
          descOpt() => t.resolveFor(ChildModel2);
          desc() => descOpt().get();
          mm() => desc().memberDescriptorsMap;
          nameConstraints() => mm()[#name].constraintDescriptors;

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
          });

          test('and provide access in members map', () {
            expect(mm(), hasLength(1));
            expect(mm()[#name], isNotNull);
            expect(nameConstraints(), hasLength(2));
          });

        });

      });
      group('should include inherited type constraints', () {
        group('when no constraints on current class', () {
          descOpt() => t.resolveFor(ChildModel3);
          desc() => descOpt().get();
          cds() => desc().constraintDescriptors;

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
          });

          test('and include in current type decriptor', () {
            expect(cds(), hasLength(1));
            expect(cds().first.constraint.validator, equals(blahIsNullOrPositive));
          });

        });

        group('when subclass has additional constraints on class', () {
          descOpt() => t.resolveFor(ChildModel4);
          desc() => descOpt().get();
          cds() => desc().constraintDescriptors;

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
          });

          test('and include in current type decriptor', () {
            expect(cds(), hasLength(2));
            expect(cds().first.constraint.validator, equals(blahIsNullOrPositive));
            expect(cds().last.constraint.validator, equals(blahInRange));
          });

        });
      });

      group('should include constraints inherited from interfaces', () {
        group('and include in member descriptors', () {
          descOpt() => t.resolveFor(ChildModel5);
          desc() => descOpt().get();
          mm() => desc().memberDescriptorsMap;
          nameCds() => mm()[#name].constraintDescriptors;
          nameMatchers() => nameCds().map((cd) => cd.constraint.validator);

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
          });

          test('and provide access in members map', () {
            expect(mm(), hasLength(1));
            expect(mm()[#name], isNotNull);
            expect(nameCds(), hasLength(2));
            expect(nameMatchers(), contains(endsWithDot));
            expect(nameMatchers(), contains(isNotNull));
          });

        });

        group('and include type constraints', () {
          descOpt() => t.resolveFor(ChildModel6);
          desc() => descOpt().get();
          cds() => desc().constraintDescriptors;

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
          });

          test('and include in current type decriptor', () {
            expect(cds(), hasLength(2));
            expect(cds().first.constraint.validator, equals(blahIsNullOrPositive));
            expect(cds().last.constraint.validator, equals(blahInRange));
          });

        });
      });

      group('should include constraints inherited from mixins', () {
        descOpt() => t.resolveFor(ChildModel7);
        desc() => descOpt().get();
        mm() => desc().memberDescriptorsMap;
        blahCds() => mm()[#blah].constraintDescriptors;
        blahMatchers() => blahCds().map((cd) => cd.constraint.validator);

        group('and include in member descriptors', () {

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
          });

          test('and provide access in members map', () {
            expect(mm(), hasLength(1));
            expect(mm()[#blah], isNotNull);
            expect(blahCds(), hasLength(1));
            expect(blahMatchers(), contains(isNotNull));
          });

        });

        group('and include type constraints', () {
          cds() => desc().constraintDescriptors;

          test('', () {
            expect(descOpt(), new isInstanceOf<Some>());
          });

          test('and include in current type decriptor', () {
            expect(cds(), hasLength(1));
            expect(cds().first.constraint.validator, equals(blahIsNullOrPositive));
          });

        });

      });

      group('should detect collections', () {
        descOpt() => t.resolveFor(TestModel4);
        desc() => descOpt().get();
        mm() => desc().memberDescriptorsMap;
        modelsMemDesc() => mm()[#models];
        modelsTypeDescOpt() => modelsMemDesc().typeDescriptor;
        modelsTypeDesc() => modelsTypeDescOpt().get();
        modelsCds() => modelsMemDesc().constraintDescriptors;
        blahMatchers() => modelsCds().map((cd) => cd.constraint.validator);

        test('', () {
          expect(descOpt(), new isInstanceOf<Some>());
          expect(mm(), hasLength(1));
          expect(modelsMemDesc(), isNotNull);
        });

        test('and set isMultivalued', () {
          expect(modelsMemDesc().isMultivalued, equals(true));
        });

        test('and set isGeneric', () {
          expect(modelsMemDesc().isGeneric, equals(true));
        });

        test('and have type that is the generic type', () {
          expect(modelsTypeDescOpt(), new isInstanceOf<Some>());
          expect(modelsTypeDesc().type, equals(TestModel1));
        });

      });
    });

    group('should ignore private members', () {
      descOpt() => t.resolveFor(PrivateField);
      desc() => descOpt().get();
      mm() => desc().memberDescriptorsMap;
      ageMemDesc() => mm()[#age];

      test('', () {
        expect(descOpt(), new isInstanceOf<Some>());
        expect(mm(), hasLength(1));
        expect(ageMemDesc(), isNotNull);
      });

    });

}


